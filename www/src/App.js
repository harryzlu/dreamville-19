import React, { useContext } from 'react';
import logo from './logo.svg';
import Layout from './components/Layout/Layout';
import { StateProvider } from './containers/State';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Start from './components/Start';
import Family from './components/Family';
import Login from './components/Login/Login';
import Finances from './components/Finances';
import MovingExpenses from './components/MovingExpenses/MovingExpenses';

function App() {
  const initialState = {
    isAuthenticated: false,
    transactions: null,
    destTransactions: null,
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case ('login'):
        return {
          ...state,
          isAuthenticated: true
        };
      case ('logout'):
        return {
          ...state,
          isAuthenticated: false
        };
      case ('transactions'):
        return  {
          ...state,
          transactions: action.data
        }
      case ('dest-transactions'):
        return  {
          ...state,
          destTransactions: action.data
        }
      default:
        return state;
    }
  };

  return (
    <div className="App">
      <StateProvider initialState={initialState} reducer={reducer}>
        <Router>
          <Layout>
            <Switch>
              <Route exact path="/" component={Start}/>
              <Route path="/family" component={Family}/>
              <Route path="/login" component={Login}/>
              <Route path="/finances" component={Finances}/>
              <Route path="/moving-expenses" component={MovingExpenses}/>
            </Switch>
          </Layout>
        </Router>
      </StateProvider>
    </div>
  );
}

export default App;
