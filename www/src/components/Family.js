import React from 'react';
import { Link } from 'react-router-dom';
import LeftCard from './LeftCard/LeftCard';
import ProgressCard from './ProgressCard/ProgressCard';
import { Select, InputLabel, FormControl, MenuItem, Fab, TextField } from '@material-ui/core';

export default () => {
    return <div className="start-page">
        <LeftCard>
            <h1>You and your family</h1>
            <h4>How many of you are moving?</h4>
            <TextField
                label="Adults"
                placeholder="0"
                className="input-field"
            />
            <br/>
            <TextField
                label="Children"
                placeholder="0"
                className="input-field"
            />
            <h4>Tell us about your household finances</h4>
            <FormControl>
                <InputLabel shrink htmlFor="incomes">Number of incomes</InputLabel>
                <Select
                    inputProps={{
                        name: 'incomes',
                        id: 'incomes',
                    }}
                    displayEmpty
                    className="input-field"
                    value='Select'
                >
                    <MenuItem value='Select'>Select</MenuItem>
                    <MenuItem value='Single'>Single Income</MenuItem>
                    <MenuItem value='Dual'>Dual Income</MenuItem>
                    <MenuItem value='Other'>Other</MenuItem>
                </Select>
            </FormControl>
            <br/>
            <FormControl>
                <InputLabel shrink htmlFor="profession">Profession</InputLabel>
                <Select
                    inputProps={{
                        name: 'profession',
                        id: 'profession',
                    }}
                    displayEmpty
                    className="input-field"
                    value='Select'
                >
                    <MenuItem value='Select'>Select</MenuItem>
                    <MenuItem value='Pirate'>Pirate</MenuItem>
                    <MenuItem value='Equestrian'>Equestrian</MenuItem>
                    <MenuItem value='Bagel'>Bagel</MenuItem>
                </Select>
            </FormControl>
            <br/>
            <TextField
                label="Current annual income"
                placeholder="0"
                className="input-field"
            />
            <br/>
            <Link to="/login" className="continue-button">
                <Fab
                    variant="extended"
                    color="inherit"
                    className="primary-button"
                    >
                    Continue
                </Fab>
            </Link>
        </LeftCard>
        <ProgressCard step={2}></ProgressCard>
    </div>
}