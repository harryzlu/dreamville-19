import React from 'react';
import { Link } from 'react-router-dom';
import CustomerTransactions from '../containers/CustomerTransactions';
import { useStateValue } from '../containers/State';
import _ from 'lodash';
import Iframe from 'react-iframe'
import Paper from '@material-ui/core/Paper';
import { Card, Fab } from '@material-ui/core';

const getMonthlySpending = (txns) => {
    const monthly = {};

    txns.forEach(txn => {
        const mn = txn['originationDateTime'].substring(0, 7);
        if (monthly[mn]) {
            monthly[mn] += txn['currencyAmount'];
        } else {
            monthly[mn] = txn['currencyAmount'];
        }
    });

    let sum = 0, count = 0;
    for (let key in monthly) {
        count++;
        sum += monthly[key];
    }
    return (sum/count).toFixed(2);
}

const getTxnsByType = (txns, typeCode) => {
    return txns.filter(txn => {
        return txn['categoryTags']
    })
}

export default () => {
    const [{ transactions, destTransactions }, dispatch] = useStateValue();

    return <div>
        {/* <CustomerTransactions
            custId="c3c6f89e-0920-41c7-b585-c7018042ffd1"
            destCustId="06440fd0-f424-444a-93e2-706aab216dfe">
            FINANCES
           
        </CustomerTransactions> */}
        {/* <iframe width="600" height="450" src= frameborder="0" style="border:0" allowfullscreen></iframe> */}


<Paper>

<Iframe url="https://datastudio.google.com/embed/reporting/1ZDZX44VBL5nZnNED9LnhIfnZthduZDE2/page/k3v0"
        width="750px"
        height="450px"
        id="myId"
        className="myClassname"
        display="initial"
        allow="fullscreen"
        position="relative"/>



{/* <div>




<Link to="/moving-expenses">
                See Moving Expenses
            </Link>
            <Link to="/">
                Create a Budget
            </Link>
</div> */}






</Paper>


<div styles={{  "display": "flex",
  "justifyContent": "center",
  "alignItems": "center"}}>
            <Link to="/moving-expenses" className="moving-page__button">
                <Fab
                    variant="extended"
                    color="inherit"
                    className="secondary-button"
                    >
                    See Moving Expenses
                </Fab>
            </Link>
            <Link to="/" className="moving-page__button">
                <Fab
                    variant="extended"
                    color="inherit"
                    className="primary-button"
                    >
                   Create a Budget
                </Fab>
            </Link>
        </div>

        
    </div>
}