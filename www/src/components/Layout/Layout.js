import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { useStateValue } from '../../containers/State';
import { Toolbar } from '@material-ui/core';

import './Layout.scss';

export default (props) => {
    const [{ isAuthenticated }, dispatch] = useStateValue();
    return (
        <div className="layout">
            <AppBar position="relative" color="inherit" className="app-bar">
                <Toolbar>
                    <Link to="/" className="heading--text app-bar__logo">
                        <span>
                            SmoothMove
                        </span>
                    </Link>
                    <div className="app-bar__links">
                        <Link to="/">
                            <span className="heading--text underlined">
                                Plan
                            </span>
                        </Link>
                        <FontAwesomeIcon icon={faChevronDown} />
                        <Link to="/">
                            <span className="heading--text">
                                Budget
                            </span>
                        </Link>
                        <FontAwesomeIcon icon={faChevronDown} />
                    </div>
                    {isAuthenticated && (
                        <div>
                            <span className="app-bar__name">
                                Bobby B.
                            </span>
                            <FontAwesomeIcon icon={faChevronDown} />
                        </div>
                    )}
                </Toolbar>
            </AppBar>
            {props.children}
        </div>
    );
}