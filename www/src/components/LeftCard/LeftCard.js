import React from 'react';
import { Card } from '@material-ui/core';
import './LeftCard.scss';

export default ({ children }) => {
    return (
        <Card className="left-card">
            {children}
        </Card>
    )
}