import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';
import LeftCard from '../LeftCard/LeftCard';
import ProgressCard from '../ProgressCard/ProgressCard';
import { Select, InputLabel, MenuItem, Fab, TextField,Button, Card, CardContent, Checkbox, Divider, FormControl, FormControlLabel, Typography } from '@material-ui/core';

import './Login.scss';

export default () => {
    const [isModalOpen, setIsModalOpen] = useState(false);

    return <div className="login-page">
        <LeftCard>
            <h1>Connect your bank</h1>
            <p>Connecting your bank allows us to accurately predict how your finances will change when you move</p>
            <div className="login-page__banks">
                <a onClick={() => setIsModalOpen(true)}>
                    <div className="login-page__bank">
                        <img className="login-page__bank-image" src="https://upload.wikimedia.org/wikipedia/commons/a/a4/Toronto-Dominion_Bank_logo.svg" alt="logo"/>
                        <span>TD Canada Trust</span>
                    </div>
                </a>
            </div>
        </LeftCard>
        <ProgressCard step={3}></ProgressCard>
        <Modal
          isOpen={isModalOpen}
          onRequestClose={() => setIsModalOpen(false)}
          className="login-modal"
        >
            <Card className="login-modal__card">
                <img className="w-16 m-16" src="https://upload.wikimedia.org/wikipedia/commons/a/a4/Toronto-Dominion_Bank_logo.svg" alt="logo"/>

                <h4>TD Canada Trust</h4>

                <TextField
                    className="login-modal__field"
                    label="Debit card number or username"
                    autoFocus
                    type="Debit card number or username"
                    name="Debit card number or username"
                    variant="outlined"
                    required
                    fullWidth
                />

                <TextField
                    className="login-modal__field"
                    label="Password"
                    type="password"
                    name="password"
                    variant="outlined"
                    required
                    fullWidth
                />

                <Link to="/finances">
                    <Fab
                        variant="extended"
                        color="primary"
                        className="primary-button"
                        >
                        Submit
                    </Fab>
                </Link>

            </Card>
        </Modal>
    </div>
}