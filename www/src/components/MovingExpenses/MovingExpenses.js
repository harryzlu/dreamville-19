import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import { Card, Fab } from '@material-ui/core';

import './MovingExpenses.scss';

export default () => {
    return <div class="moving-page">
        <h1>Your predicted moving expenses</h1>
        <div class="space-between moving-page__summary">
            <h2>
                Winnipeg <FontAwesomeIcon icon={faLongArrowAltRight} /> Toronto
            </h2>
            <div>
                <span>2 adults, 1 child </span>
                <a className="tertiary-link">Change</a>
            </div>
        </div>
        <Card className="moving-page__cost space-between">
            <h3>Flights from Winnipeg to Toronto</h3>
            <div className="moving-page__total">
                <div className="moving-page__currency">$501.00</div>
                <div className="moving-page__links">
                    <a className="tertiary-link">Edit</a>
                    <span>|</span>
                    <a className="tertiary-link">Show me deals</a>
                </div>
            </div>
        </Card>
        <Card className="moving-page__cost space-between">
            <h3>Apartment First and Last payment (2 bedroom)</h3>
            <div className="moving-page__total">
                <div className="moving-page__currency">$3987.00</div>
                <div className="moving-page__links">
                    <a className="tertiary-link">Edit</a>
                    <span>|</span>
                    <a className="tertiary-link">Show me deals</a>
                </div>
            </div>
        </Card>
        <Card className="moving-page__cost space-between">
            <h3>Temporary accomodation</h3>
            <div className="moving-page__total">
                <div className="moving-page__currency">$2733.00</div>
                <div className="moving-page__links">
                    <a className="tertiary-link">Edit</a>
                    <span>|</span>
                    <a className="tertiary-link">Show me deals</a>
                </div>
            </div>
        </Card>
        <Card className="moving-page__cost space-between">
            <h3>Moving truck</h3>
            <div className="moving-page__total">
                <div className="moving-page__currency">$834.00</div>
                <div className="moving-page__links">
                    <a className="tertiary-link">Edit</a>
                    <span>|</span>
                    <a className="tertiary-link">Show me deals</a>
                </div>
            </div>
        </Card>
        <div className="space-between moving-page__grand-total">
            <a className="tertiary-link">+ Add another moving expense</a>
            <div>Total <span className="moving-page__currency">$8732.00</span></div>
        </div>
        <div className="moving-page__buttons">
            <Link to="/finances" className="moving-page__button">
                <Fab
                    variant="extended"
                    color="inherit"
                    className="secondary-button"
                    >
                    Create a Budget
                </Fab>
            </Link>
            <Link to="/finances" className="moving-page__button">
                <Fab
                    variant="extended"
                    color="inherit"
                    className="primary-button"
                    >
                    Save and Continue
                </Fab>
            </Link>
        </div>
        <div className="moving-page__note">
            You will be able to come back to these details.
        </div>
    </div>
}