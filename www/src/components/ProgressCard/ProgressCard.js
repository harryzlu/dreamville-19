import React from 'react';
import { Card } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import './ProgressCard.scss';

export default (props) => {
    return (
        <Card className="progress-card">
            <div className={`progress-card__step ${props.step >= 1 ? 'progress-card__step--active' : ''}`}>
                {props.step > 1 ? <FontAwesomeIcon icon={faCheck} /> : <span>1</span>} - Your move
            </div>
            <div className={`progress-card__step ${props.step >= 2 ? 'progress-card__step--active' : ''}`}>
                {props.step > 2 ? <FontAwesomeIcon icon={faCheck} /> : <span>2</span>} - You and your family
            </div>
            <div className={`progress-card__step ${props.step >= 3 ? 'progress-card__step--active' : ''}`}>
                {props.step > 3 ? <FontAwesomeIcon icon={faCheck} /> : <span>3</span>} - Connect your bank
            </div>
        </Card>
    )
}