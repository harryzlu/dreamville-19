import React from 'react';
import { Link } from 'react-router-dom';
import LeftCard from './LeftCard/LeftCard';
import ProgressCard from './ProgressCard/ProgressCard';
import { Select, InputLabel, FormControl, MenuItem, Fab } from '@material-ui/core';

export default () => {
    return <div className="start-page">
        <LeftCard>
            <h1>Tell us about your move</h1>
            <h4>Where do you currently live?</h4>
            <FormControl>
                <InputLabel shrink htmlFor="current-country">Country</InputLabel>
                <Select
                    inputProps={{
                        name: 'current-country',
                        id: 'current-country',
                    }}
                    displayEmpty
                    className="input-field"
                    value='Select'
                >
                    <MenuItem value='Select'>Select</MenuItem>
                    <MenuItem value='Canada'>Canada</MenuItem>
                </Select>
            </FormControl>
            <br/>
            <FormControl>
                <InputLabel shrink htmlFor="current-city">City</InputLabel>
                <Select
                    inputProps={{
                        name: 'current-city',
                        id: 'current-city',
                    }}
                    displayEmpty
                    className="input-field"
                    value='Select'
                >
                    <MenuItem value='Select'>Select</MenuItem>
                    <MenuItem value='Winnipeg'>Winnipeg</MenuItem>
                </Select>
            </FormControl>
            <h4>Where are you moving to?</h4>
            <FormControl>
                <InputLabel shrink htmlFor="dest-country">Country</InputLabel>
                <Select
                    inputProps={{
                        name: 'dest-country',
                        id: 'dest-country',
                    }}
                    displayEmpty
                    className="input-field"
                    value='Select'
                >
                    <MenuItem value='Select'>Select</MenuItem>
                    <MenuItem value='Canada'>Canada</MenuItem>
                </Select>
            </FormControl>
            <br/>
            <FormControl>
                <InputLabel shrink htmlFor="dest-city">City</InputLabel>
                <Select
                    inputProps={{
                        name: 'dest-city',
                        id: 'dest-city',
                    }}
                    displayEmpty
                    className="input-field"
                    value='Select'
                >
                    <MenuItem value='Select'>Select</MenuItem>
                    <MenuItem value='Toronto'>Toronto</MenuItem>
                </Select>
            </FormControl>
            <br/>
            <Link to="/family" className="continue-button">
                <Fab
                    variant="extended"
                    color="inherit"
                    className="primary-button"
                    >
                    Continue
                </Fab>
            </Link>
        </LeftCard>
        <ProgressCard step={1}></ProgressCard>
    </div>
}