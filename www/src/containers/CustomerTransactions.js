import React, { Component } from 'react';
import axios from 'axios';
import { StateContext } from './State';

const URL = 'https://api.td-davinci.com/api/customers/{custId}/transactions';

export default class CustomerTransactions extends Component {
    static contextType = StateContext;

    componentDidMount() {
        axios.get(URL.replace('{custId}', this.props.custId), {
            headers: { 'Authorization': process.env.REACT_APP_DA_VINCI_KEY }
        }).then(res => {
            const [_, dispatch] = this.context;
            dispatch({
                type: 'transactions',
                data: res.data.result
            })
        });

        axios.get(URL.replace('{custId}', this.props.destCustId), {
            headers: { 'Authorization': process.env.REACT_APP_DA_VINCI_KEY }
        }).then(res => {
            const [_, dispatch] = this.context;
            dispatch({
                type: 'dest-transactions',
                data: res.data.result
            })
        });
    }

    render() {
        const [{ transactions, destTransactions }, dispatch] = this.context;
        return (transactions && destTransactions) ? <div>{this.props.children}</div> : null;
    }
}